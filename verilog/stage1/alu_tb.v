// Design:      Simple RPN calculator
// File:        alu.v
// Description: Arithmetic-(Logic) Unit. Test bench.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-27 (initial)

`timescale 1ns / 1ps

// Simulation macros and default values:
//   NP: number of test patterns
//   SEED: initial seed for pseudo-random number generation
//   OP: type of operation (see 'alu.v')

`ifndef NP
    `define NP 20
`endif
`ifndef SEED
    `define SEED 1
`endif
`ifndef OP
    `define OP 0
`endif

module test ();
    
    reg signed [7:0] a;         // input 'a'
    reg signed [7:0] b;         // input 'b'
    reg [1:0] op = `OP;         // type of operation
    wire signed [7:0] alu_out;  // output
    wire c;                // carry flag
    wire v;                // overflow flag

    integer np;                 // number of patterns (auxiliary signal)
    integer seed = `SEED;       // seed (auxiliary signal)

    // Circuit under test
    /* Alu width is parametrized. We instantiate an 8 bit ALU. */
    alu #(.W(8)) uut (
        .a(a),
        .b(b),
        .op(op), 
        .c(c), 
        .v(v),
        .r(alu_out)
        );

    initial begin
        /* 'np' is a counter that holds the remaining number of patterns
         * to be applied. The initial value is taken from macro NP. */
        np = `NP;
        
        // Waveform generation (optional)
        // $dumpfile("test.vcd");
        // $dumpvars(0, test);
        
        // Output printing
        $write("Operation: %d ", op);
        case (op)
        0:       $display("ADD");
        1:       $display("SUB");
        2:       $display("TRANSFER A");
        default: $display("NEGATE B");
        endcase

        /* Inputs and output are displayed in decimal and in binary
         * formats so that both arithmetic and logic operations can be
         * esily checked. */
        $display("       A                B",
                 "               OUT         c  v");
        $monitor("%b (%d)  %b (%d)   %b (%d)  %b  %b",
                   a, a, b, b, alu_out, alu_out, c, v);
    end

    // Test generation process
    /* Each 20ns 'a' and 'b' are assigned random values. Simulation
     * ends after applying NP patterns. The pattern sequence can be 
     * changed by defining a different value for SEED. Macros NP and SEED
     * can be changed in the file or using compilation options. */
    always begin
        #20
        a = $random(seed);
        b = $random(seed);
        np = np - 1;
        if (np == 0)
            $finish;
    end
endmodule

/*
   Test with other parameters:
   
   $ iverilog -DOP=1 -DNP=40 -DSEED=2 alu.v alu_tb.v
   $ vpp a.out
 */
