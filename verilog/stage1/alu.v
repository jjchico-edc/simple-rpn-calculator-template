// Design:      Simple RPN calculator
// File:        alu.v
// Description: Arithmetic-(Logic) Unit
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-27 (initial)

`define ALU_ADD  0
`define ALU_SUB  1
`define ALU_TRA  2
`define ALU_NEGB 3

module alu #(
    parameter W = 8         // Data width
)(
    input wire [W-1:0] a,   // data input a
    input wire [W-1:0] b,   // data input b
    input wire [1:0] op,    // operation selector
    output reg c,           // carry carry output flag
    output reg v,           // overflow flag
    output wire [W-1:0] r   // output result
);

    /* ----------------------------------------------- */

endmodule
