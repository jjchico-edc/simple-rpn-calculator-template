// Design:      Simple RPN calculator
// File:        globals.vh
// Description: Global macros and definitions
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-27 (initial)

`define ALU_ADD  0
`define ALU_SUB  1
`define ALU_TRA  2
`define ALU_NEGB 3
