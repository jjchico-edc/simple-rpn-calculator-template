// Design:      Simple RPN calculator
// File:        calculator.v
// Description: Whole calculator (control + data units).
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-28 (initial)

module calculator #(
    parameter W = 8     // Data width
)(
    input wire clk,             // clock (rising edge)
    input wire reset,           // reset (synchronous, active-low)
    input wire enter,           // enter key
    input wire add,             // add key
    input wire sub,             // subtract key
    input wire pm,              // plus/minus key
    input wire [W-1:0] x_in,    // data input
    output wire [W-1:0] yreg,   // data output (Y register)
    output wire f_c,            // flag carry
    output wire f_v             // flag overflow
);

    /* ------------------------------------------ */
    
endmodule
