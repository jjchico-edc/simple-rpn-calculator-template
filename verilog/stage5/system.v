// Design:      Simple RPN calculator
// File:        system.v
// Description: Calculator system for development board implementation.
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-03-01 (initial)

module system (
    input wire clk,             // clock (rising edge)
    // input wire reset,           // reset (synchronous, active-low)
    input wire enter,           // enter key
    input wire add,             // add key
    input wire sub,             // subtract key
    input wire pm,              // plus/minus key
    input wire [7:0] x_in,      // data input
    output wire f_c,            // flag carry
    output wire f_v,            // flag overflow
    output wire [3:0] an,       // 7-segment anode control
    output wire [0:6] seg,      // 7-segment code (active-low)
    output wire dp              // 7-segment decimal point output
);

    /* --------------------------------------- */

endmodule
