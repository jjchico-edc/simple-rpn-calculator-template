// Design:      Simple RPN calculator
// File:        data_unit.v
// Description: Data Unit
// Author:      Jorge Juan-Chico <jjchico@dte.us.es>
// Date:        2021-02-27 (initial)

module data_unit #(
    parameter W = 8     // Data width
)(
    input wire clk,
    input wire [W-1:0] x_in,
    input wire [1:0] op,
    input wire y_w,
    input wire s_w,
    output reg [W-1:0] yreg,       // Y register
    output wire f_c,             // flag carry
    output wire f_v              // flag overflow
);

    /* ---------------------------------- */

endmodule
